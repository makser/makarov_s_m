#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <string>
#include <vector>
#include "opencv2/opencv.hpp"
#include "../include/LaneDetector.hpp"
#include "../LaneDetector/LaneDetector.cpp"


int main(int argc, char *argv[]) {
	setlocale(LC_ALL, "Russian");
	std::cout << "������� ������ ���� �� ������������ �����\n";
    std::string source;
	std::cin >> source;
    cv::VideoCapture cap(source);
    if (!cap.isOpened())
      return -1;

    LaneDetector lanedetector; 
    cv::Mat frame;
    cv::Mat img_denoise;
    cv::Mat img_edges;
    cv::Mat img_mask;
    cv::Mat img_lines;
    std::vector<cv::Vec4i> lines;
    std::vector<std::vector<cv::Vec4i> > left_right_lines;
    std::vector<cv::Point> lane;
	cv::Point intersect_point;
	float IoU = 0.0;
    int flag_plot = -1;
    int i = 0;
	int j = 0;
	double vanish_x;
	std::string path = "C:/Users/makar/Downloads/Coursework/images/standards.json";

    
    while (i < 540) {

      if (!cap.read(frame))
        break;

	  /*if (i % 10 == 0) {
		  cv::imwrite("C:/Users/makar/Downloads/Coursework/images/example" + std::to_string(j) + ".png", frame);
		  j++;
	  }*/

      img_denoise = lanedetector.deNoise(frame);
      
      img_edges = lanedetector.edgeDetector(img_denoise);
      
      img_mask = lanedetector.mask(img_edges);
	  
      lines = lanedetector.houghLines(img_mask);

      if (!lines.empty()) {
        left_right_lines = lanedetector.lineSeparation(lines, img_edges);

        lane = lanedetector.regression(left_right_lines, frame);
		
		intersect_point = lanedetector.vanishPoint();

        flag_plot = lanedetector.plotLane(frame, lane);

		if (i % 10 == 0) {
			cv::FileStorage fs(path, 0);
			std::vector<std::string> names;
			cv::FileNode files = fs["_via_img_metadata"];
			read(fs["_via_image_id_list"], names);
			std::string file_name = names[j];
			cv::FileNode shape = files[file_name]["regions"][0]["shape_attributes"];
			std::vector<int> xs, ys;
			read(shape["all_points_x"], xs);
			read(shape["all_points_y"], ys);
			cv::Point2i standardPoints[1][3] = {};
			for (int k = 0; k < xs.size(); k++)
			{
				standardPoints[0][k].x = xs[k];
				standardPoints[0][k].y = ys[k];
			}
			fs.release();
			IoU += lanedetector.IoU(lane, intersect_point, standardPoints);
			j++;
		}
		
        i += 1;
        cv::waitKey(25);
      } else {
          flag_plot = -1;
      }
    }
	IoU /= j + 1;
	std::cout << IoU << std::endl;
    return flag_plot;
}
