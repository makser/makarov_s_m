#pragma once
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include "opencv2/opencv.hpp"


class LaneDetector {
 private:
  double img_size;
  double img_center;
  bool left_flag = false;  
  bool right_flag = false;  
  cv::Point right_b;  
  double right_m;  
  cv::Point left_b; 
  double left_m;  

 public:
  cv::Mat deNoise(cv::Mat inputImage); 
  cv::Mat edgeDetector(cv::Mat img_noise);  
  cv::Mat mask(cv::Mat img_edges);  
  std::vector<cv::Vec4i> houghLines(cv::Mat img_mask);  
  std::vector<std::vector<cv::Vec4i> > lineSeparation(std::vector<cv::Vec4i> lines, cv::Mat img_edges);  
  std::vector<cv::Point> regression(std::vector<std::vector<cv::Vec4i> > left_right_lines, cv::Mat inputImage);  
  cv::Point vanishPoint();
  float IoU(std::vector<cv::Point> points, cv::Point intersct,cv::Point arr[1][3]);
  int plotLane(cv::Mat inputImage, std::vector<cv::Point> lane);  
};
